# Testbeam Analysis

Testbeam analysis related scripts and configurations

## Getting started

Subfolder March 2022 contains an initial version of Fergus configurations, modified to run without a metronome to generate event times

### Installing corry anywhere

Checkout corry by doing:

```bash
git clone https://gitlab.cern.ch/atlaspix3/corryvreckan.git
cd corryvreckan
git checkout ATLASPIX3_UL_Mac
mkdir build
cd build
```

When compiling corry, in its build directory run cmake as following:

```
cmake -DBUILD_EventLoaderATLASPix3=ON ../
make -j6 install
```

This should enable compilation of the ATLASPix3 event loader and get you going

Once done with that, add corry to your path (makes scripts work):

```bash
export PATH=$PATH:`pwd`
```

### Get the analysis related scripts

Clone this repo
```bash
git clone https://gitlab.cern.ch/atlaspix3/atlaspix3_telescope_decoding.git
cd atlaspix3_telescope_decoding
```

To prevent `chmod` changes to be shown, do ``git config core.fileMode false``


If you now go into a run subfolder of this project ``cd march2022``, you should be able to
- run the `telescopeAlign.sh` script to do tracking with all layers.
- Or use `tel-dut-Align.sh` to use one layer as the DUT to run analyses on. Remember to uncomment `role = "dut"` in the `march2022/geometries/start.conf`.

```bash
bash ../scripts/telescopeAlign.sh <DATAFILE>
```

Output files (mask configurations as well as root files) get generated in:

```
output/march2022
```

Geometrie files that are derived from alignment runs and such will be stored in:

```
geometries/derived
```
