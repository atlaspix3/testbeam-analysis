# README

Kindly provided by Fergus Wilson

## Decoding binary telescope data for use in corry

`decode.conf`: file used by udp decoder to convert binary to text fiel data/test.dat

`atlaspix3_decoder decode.conf`

`data/test.dat` : o/p of the first few hits from the udp decoder

## Running corry

For below to work ok, your favourite corry binary should be in your path.

`ATLASPix3_telescope_testbeam.conf` : read in usp decoded text file
(must be in data/test.dat) and try and prealign. 

`geometry/telescope_Apr2022_initial_nsens4.geo` : definition of 4 sensor
telescope; settings are a guess at this point.

`corry -c ATLASPix3_telescope_testbeam.conf`
