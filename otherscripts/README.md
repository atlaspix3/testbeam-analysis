# READ ME

Scripts that are non-reco scripts.

- `extTSProg.py` diffs extTS from the first extTS, makes plots, python3
- `extTSRowDiff.py` diffs extTS line by line, outputs diffs and extTSRowDiff exceeding 1e9, makes plots, python3
- `cutTSjump.py` takes a decoded input file and outputs a file without extTS jumps larger than 1e7
- `runAllExtTSProg.sh` batch run `extTSProg.py` on all specified data
- `runAllExtTSRowDiff.sh` batch run `extTSRowDiff.py` on all specified data



```
python checkExtTS.py /eos/user/a/atlaspix/202204TB_data/Lancs/20220410030_udp_interleaved_lancs_2_decode.dat [outdir]")
```
