#!/bin/python

### Uses several times filesize of memory!
### lmeng@cern.ch

import sys
import pandas as pd
import os.path

if __name__ == '__main__':
	if len(sys.argv) < 3:
		print("input file or output path needed! e.g.")
		print("python cutTSjump.py /eos/user/a/atlaspix/202204TB_data/Lancs/202204082012_udp_beamonallquad_vminus90_6GeV_lancs_0_decode.dat /eos/user/a/atlaspix/202204TB_data/Lancs/")
		exit()
	
	outdir = ""

	outdir = sys.argv[2]
	if not outdir.endswith("/"):
		outdir += "/"

	infile = sys.argv[1]
	basename = os.path.basename(infile).split(".dat")[0]
	print("--------------------> {}".format(basename))
	
	## need to keep the semicolon in the column names for reco
	data = pd.read_csv(infile, skiprows=1, names = ["# PackageID;", "Layer;", "Column;", "Row;", "TS;", "TS1;", "TS2;", "TriggerTS;", "TriggerID;", "ext. TS;", "ext. TS2;", "FIFO overflow"], sep="\t")

	lendata = len(data)
	print(data.head())
	print(data.info())
	
	## for some reason copies the column names from the original dataframe
	# data2 = pd.DataFrame(columns=["# PackageID;", "Layer;", "Column;", "Row;", "TS;", "TS1;", "TS2;", "TriggerTS;", "TriggerID;", "ext. TS;", "ext. TS2;", "FIFO overflow"])
	data2 = pd.DataFrame()
	data2 = pd.concat([data.iloc[0:1], data[abs(data["ext. TS;"].diff()) < 1e7]], ignore_index=True)
	
	print(data2.head())
	print(data2.info())
	
	print(lendata, len(data2))
	data2.to_csv(outdir+basename+"_cut.dat", sep="\t", index=False)
