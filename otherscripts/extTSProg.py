import sys
import matplotlib as mpl
# mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import gc
import os.path

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print("input file needed! e.g.")
		print("python extTSprog.py /eos/user/a/atlaspix/202204TB_data/Lancs/202204061308_udp_beamonall_6GeV_lancs_0_decode.dat [outdir]")
		exit()
	
	outdir = ""
	
	if len(sys.argv) >= 3:
		outdir = sys.argv[2]
		if not outdir.endswith("/"):
			outdir += "/"

	infile = sys.argv[1]
	basename = os.path.basename(infile).split(".dat")[0]
	print("--------------------> {}".format(basename))
	
	"""
		# # # for this file: /eos/user/a/atlaspix/202204TB_data/Lancs/202204061308_udp_beamonall_6GeV_lancs_0_decode.dat
		data = pd.read_csv(infile, skiprows=1, names = ["# PackageID", "Layer", "Column", "Row", "TS", "TS1", "TS2", "TriggerTS", "TriggerID", "ext. TS", "ext. TS2", "FIFO overflow"], sep="\t", nrows=600000)
	"""
	
	data = pd.read_csv(infile, skiprows=1, names = ["# PackageID", "Layer", "Column", "Row", "TS", "TS1", "TS2", "TriggerTS", "TriggerID", "ext. TS", "ext. TS2", "FIFO overflow"], sep="\t")
	
	lendata = len(data)
	# print(data.head())
	print(data.info())
	
	data["ext. TS prog"] = data["ext. TS"] - data.at[0,"ext. TS"]
	print(data.head())

	fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(16,8))
	
	"""
		# # # use for this file: /eos/user/a/atlaspix/202204TB_data/Lancs/202204061308_udp_beamonall_6GeV_lancs_0_decode.dat
		data.iloc[425000:525000].plot(y="ext. TS prog", kind='line', style=".", markersize=1, xlabel="Data Row", ylabel="ext. TS progress (TS)", ax=axes[0], grid=True, title=basename)
		data["ext. TS"]=data["ext. TS"]*25e-9
		data.iloc[425000:525000].plot(x="ext. TS", y="ext. TS prog", kind='scatter', style=".", s=1, xlabel="Time (s)", ylabel="ext. TS progress (TS)", ax=axes[1], grid=True)
	"""
	
	data.plot(y="ext. TS prog", kind='line', style=".", markersize=1, xlabel="Data Row", ylabel="ext. TS progress (TS)", ax=axes[0], grid=True, title=basename)
	data["ext. TS"]=data["ext. TS"]*25e-9
	data.plot(x="ext. TS", y="ext. TS prog", kind='scatter', style=".", s=1, xlabel="Time (s)", ylabel="ext. TS progress (TS)", ax=axes[1], grid=True)
	plt.tight_layout()
	plt.savefig(outdir+basename+"_prog.png")
	
	plt.show()
