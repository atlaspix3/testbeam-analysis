import sys
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import gc
import os.path

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print("input file needed! e.g.")
		print("python extTSRowDiff.py /eos/user/a/atlaspix/202204TB_data/Lancs/20220410030_udp_interleaved_lancs_2_decode.dat [outdir]")
		exit()
	
	outdir = ""
	
	if len(sys.argv) >= 3:
		outdir = sys.argv[2]
		if not outdir.endswith("/"):
			outdir += "/"

	infile = sys.argv[1]
	basename = os.path.basename(infile).split(".dat")[0]
	print("--------------------> {}".format(basename))

	data = pd.read_csv(infile, skiprows=1, names = ["# PackageID", "Layer", "Column", "Row", "TS", "TS1", "TS2", "TriggerTS", "TriggerID", "ext. TS", "ext. TS2", "FIFO overflow"], sep="\t", usecols=["ext. TS"])
	# data = pd.read_csv(infile, skiprows=1, names = ["# PackageID", "Layer", "Column", "Row", "TS", "TS1", "TS2", "TriggerTS", "TriggerID", "ext. TS", "ext. TS2", "FIFO overflow"], sep="\t", usecols=["TriggerTS"], nrows=500)
	
	lendata = len(data)
	# print(data.head())
	print(data.info())
	
	rowdiff = pd.DataFrame(columns=["ext. TS diff"])
	rowdiff["ext. TS diff"] = data["ext. TS"].diff()
		
	print(rowdiff.info())
	del data
	gc.collect()		
	
	rowdiff = rowdiff[rowdiff["ext. TS diff"] != 0]
	print(rowdiff.describe())
	
	rowdiff.to_csv(outdir+basename+".diff")
	
	print("---> Check for time jumps...")
	exceeds = rowdiff[rowdiff["ext. TS diff"] > 1e9]
	if len(exceeds) > 0:
		exceeds.to_csv(outdir+basename+".exceed", sep="\t")
	
	print("lendata: {}; len(rowdiff): {}; len(exceed): {}".format(lendata, len(rowdiff), len(exceeds)))
	
	rowdiff.plot(kind='line',style=".", markersize=1, xlabel="Row", ylabel="ext. TS diff (TS)", figsize=(16,6), grid=True)
	plt.tight_layout()
	plt.savefig(outdir+basename+"_line.png")
	
	rowdiff.abs().plot(kind='line',style=".", markersize=1, xlabel="Row", ylabel="ext. TS diff (TS)", figsize=(16,6), grid=True, logy=True)
	plt.tight_layout()
	plt.savefig(outdir+basename+"_line_abs.png")

	rowdiff.plot(kind='hist', bins=np.linspace(int(rowdiff["ext. TS diff"].min()), int(rowdiff["ext. TS diff"].max()), 50), grid=True, logy=True)
	plt.tight_layout()
	plt.savefig(outdir+basename+"_hist.png")
	
	rowdiff.abs().plot(kind='hist', bins=np.logspace(0, 14, 29), grid=True, logx=True, logy=True)
	plt.tight_layout()
	plt.savefig(outdir+basename+"_hist_abs.png")
	
	# plt.show()
