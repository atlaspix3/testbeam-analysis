#!/bin/bash

for inst in "KIT" "Lancs"
do
	FILES="/eos/user/a/atlaspix/202204TB_data/$inst/*_decode.dat"
	OUTDIR="/eos/user/a/atlaspix/202204TB_data/$inst/"

	for file in $FILES
	do
	echo $(date +"%Y-%m-%d %T") $inst $file
	python cutTSjump.py $file $OUTDIR

	done
done
