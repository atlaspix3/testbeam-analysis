#!/bin/bash

FILES="/eos/user/a/atlaspix/202204TB_data/Lancs/*_decode.dat"
#OUTDIR="/eos/user/a/atlaspix/202204TB_checks/extTS/Lancs/"
OUTDIR="/eos/user/a/atlaspix/202204TB_checks/extTS/KIT/"

for file in $FILES
do
echo $(date +"%Y-%m-%d %T") $file
python extTSRowDiff.py $file $OUTDIR

done
