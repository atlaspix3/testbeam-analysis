# Using automated scripts

Automated analysis scripts use the starting geometry file `../march2022/geometries/start.conf` and should be executed from `../march2022`.


## Using single scripts

`telescopeAlign.sh` can be executed as it is and uses all 4 telescope planes for telescope analysis.

To use `tel-dut-Align.sh` one has to uncomment `role = "dut"` for the DUT plane in `../march2022/geometries/start.conf`, the other 3 planes are used as telescope planes.


## Using batch script

The `batch.sh` script does `mkdir -p`. Due to the current data storage structure on EOS of a service account, the parent root directory is not accessible and thus would throw a `Permission denied` error.
