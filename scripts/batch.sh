if [ $# -le 2 ]; then
    echo "usage $0 script.sh <directory> [nproc]"
    exit
fi

SCRIPT=$1
RUNLIST=$2

NPROC=$(nproc)
if [ $NPROC -gt 1 ]; then
	NPROC=`expr ${NPROC} / 2`
fi

if [ $# -gt 2 ]; then
    NPROC=$3
fi
RUNNING=()

TIMESTAMP=$(date +%Y-%m-%d-%H-%M-%S)
LOGDIR=logs/$TIMESTAMP
mkdir -p logs
mkdir -p $LOGDIR


for FILE in $(ls $RUNLIST/*decode*dat); do
    if [ -f $FILE ]; then
    	  FILEBASE=`basename $FILE .dat`
        NUM=${#RUNNING[@]}
        while [ $NUM -ge $NPROC ]; do
            NUM=${#RUNNING[@]}
            for PID in ${RUNNING[@]}; do
                kill -0 $PID &> /dev/null
                if [ $? -gt 0 ]; then
                    let "NUM--"                    
                fi
            done
            if [ $NUM -ge $NPROC ]; then
               sleep 10s
            fi
        done
        TIMESTAMP=$(date +%Y-%m-%d-%H-%M-%S)
        echo "$TIMESTAMP : $SCRIPT $FILE $REFERENCE"
        bash $SCRIPT $FILE $REFERENCE > $LOGDIR/batch.$FILEBASE.log & RUNNING+=($!) 
        sleep .1s
    else
        echo "$FILE does not exist, skipping"
    fi
done



