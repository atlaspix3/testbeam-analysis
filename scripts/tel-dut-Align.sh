#!/bin/bash
### in geometries/start.conf uncomment role = "dut" for the DUT plane


if [[ $# -lt 1 ]]; then
    echo "usage: $0 <DATAFILE> <ALIGNMENTOUTPUTFILENAME>"
    echo "<DATAFILE> should be data file to be processed"
    echo "<ALIGNMENTOUTPUTFILENAME> is the file where the aligned telescope configuration is stored"
    echo " If no output file is given, a run-based file is chosen"
    exit
fi



FILE=$1
if [[ ! -f $FILE ]]; then
    echo $FILE does not exist
    exit
fi

FILEBASE=`basename $FILE .dat`
DATADIR="" #$PWD/$1

CONFIGFILE="geometries/derived/$FILEBASE/telescopeAligned.conf"
if [[ $# -gt 1 ]]; then
  CONFIGFILE=$PWD/$2
fi

PREFIX=march2022/$FILEBASE
OUTPUTDIR=output/$PREFIX
#OUTPUTDIR=/eos/user/a/atlaspix/202204TB_reco/output/$PREFIX

echo $OUTPUTDIR

mkdir -p $OUTPUTDIR
mkdir -p geometries/derived/$FILEBASE

corry -c createMasks.conf -o detectors_file="geometries/start.conf"                     -o detectors_file_updated="geometries/derived/$FILEBASE/masked.conf"  -o histogram_file="masked.root"   -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"

### prealign doesn't do tracking, specify number of events
corry -c 01_prealign.conf     -o detectors_file="geometries/derived/$FILEBASE/masked.conf"  -o detectors_file_updated="geometries/derived/$FILEBASE/align_1.conf" -o histogram_file="align_1.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"

corry -c 02_telalign.conf     -o number_of_tracks=5000 -o Tracking4D.spatial_cut_abs=2mm,2mm -o AlignmentTrackChi2.max_track_chi2ndof=100 -o Tracking4D.min_hits_on_track=2 -o detectors_file="geometries/derived/$FILEBASE/align_1.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_2.conf" -o histogram_file="align_2.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c 02_telalign.conf     -o number_of_tracks=15000 -o Tracking4D.spatial_cut_abs=1mm,1mm -o AlignmentTrackChi2.max_track_chi2ndof=50 -o Tracking4D.min_hits_on_track=3 -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_2.conf" -o histogram_file="align_2.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c 02_telalign.conf     -o number_of_tracks=30000 -o Tracking4D.spatial_cut_abs=600um,600um -o AlignmentTrackChi2.max_track_chi2ndof=50 -o Tracking4D.min_hits_on_track=3 -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_2.conf" -o histogram_file="align_2.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c 02_telalign.conf     -o number_of_tracks=60000 -o Tracking4D.spatial_cut_abs=300um,300um -o AlignmentTrackChi2.max_track_chi2ndof=50 -o Tracking4D.min_hits_on_track=3 -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_2.conf" -o histogram_file="align_2.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"

corry -c telescope_tracks.conf -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o FileWriter.file_name="telescope_tracks.root" -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"

corry -c 03_dutalign.conf -o FileReader.file_name="$OUTPUTDIR/telescope_tracks.root" -o number_of_tracks=5000 -o DUTAssociation.spatial_cut_abs=2mm,1mm -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_3.conf" -o histogram_file="align_3.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c 03_dutalign.conf -o FileReader.file_name="$OUTPUTDIR/telescope_tracks.root" -o number_of_tracks=15000 -o DUTAssociation.spatial_cut_abs=1mm,0.4mm -o detectors_file="geometries/derived/$FILEBASE/align_3.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_3.conf" -o histogram_file="align_3.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c 03_dutalign.conf -o FileReader.file_name="$OUTPUTDIR/telescope_tracks.root" -o number_of_tracks=30000 -o DUTAssociation.spatial_cut_abs=600um,200um -o detectors_file="geometries/derived/$FILEBASE/align_3.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_3.conf" -o histogram_file="align_3.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"

corry -c 04_analyse.conf     -o detectors_file="geometries/derived/$FILEBASE/align_3.conf" -o detectors_file_updated=$CONFIGFILE                                 -o histogram_file="analyse.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
