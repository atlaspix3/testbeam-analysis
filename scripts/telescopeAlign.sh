#!/bin/bash

if [[ $# -lt 1 ]]; then
    echo "usage: $0 <DATAFILE> <ALIGNMENTOUTPUTFILENAME>"
    echo "<DATAFILE> should be data file to be processed"
    echo "<ALIGNMENTOUTPUTFILENAME> is the file where the aligned telescope configuration is stored"
    echo " If no output file is given, a run-based file is chosen"
    exit
fi



FILE=$1
if [[ ! -f $FILE ]]; then
    echo $FILE does not exist
    exit
fi

FILEBASE=`basename $FILE .dat`
DATADIR=$PWD/$1

CONFIGFILE="geometries/derived/$FILEBASE/telescopeAligned.conf"
if [[ $# -gt 1 ]]; then
  CONFIGFILE=$PWD/$2
fi

PREFIX=march2022/$FILEBASE
OUTPUTDIR=$PWD/output/$PREFIX

mkdir -p $OUTPUTDIR
mkdir -p geometries/derived/$FILEBASE





mkdir -p geometries/derived

corry -c createMasks.conf -o detectors_file="geometries/start.conf"                     -o detectors_file_updated="geometries/derived/$FILEBASE/masked.conf"  -o histogram_file="masked.root"   -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c align_1.conf     -o detectors_file="geometries/derived/$FILEBASE/masked.conf"  -o detectors_file_updated="geometries/derived/$FILEBASE/align_1.conf" -o histogram_file="align_1.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c align_2.conf     -o detectors_file="geometries/derived/$FILEBASE/align_1.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_2.conf" -o histogram_file="align_2.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c align_3.conf     -o detectors_file="geometries/derived/$FILEBASE/align_2.conf" -o detectors_file_updated="geometries/derived/$FILEBASE/align_3.conf" -o histogram_file="align_3.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
corry -c analyse.conf     -o detectors_file="geometries/derived/$FILEBASE/align_3.conf" -o detectors_file_updated=$CONFIGFILE                                 -o histogram_file="analyse.root"  -o EventLoaderATLASPix3.input_filename="$FILE" -o output_directory="$OUTPUTDIR"
