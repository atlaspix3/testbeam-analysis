# ToT calibration

These are scripts for fit of ATLASPix3 tot calibration data and cut for the benchmark run <202204082012_udp_beamonallquad_Vminus90_6Gev_kit_0_decode_cut.dat>.
Scripts can be compiled and run with ROOT.

## Fit & creat file
The script fit.cpp processes the raw data and fits the curves with a log-plus-linear function (defined in func.h).

To use it, Copy fit.cpp and func.h to the same directory of the ToT calibration raw data; change input file name in fit.cpp to the proper name of the raw data; then compile and run the script with ROOT.

### Tips:

1.fit.cpp requires input file to contain ToT-voltage curves of each pixel with errors of ToT, in the form:

```bash
# PointCurve (<column>|<row>)
# [lines for description, not necessary]
0[injected voltage, unit: V] 0[ToT, unit: period] 0[error of ToT]
0.0705882	0	0
……
0.564706	108	0.162085

# PointCurve (<column>|<row>)
……
```

2.Output file should have file name: tot_calibration_layer_[1~4].dat , which is required for corry to load it. Change the layer number before running the script, or rename the output file afterwards.

3.Limits set to the fit parameters might be invalid and adjustments might be required, especially for the parameter x0.

## Adjust the configuration files
To apply ToT calibration, a few adjustments to the default configuration files are required.

In [EventLoaderATLASPix3] module,  add lines:

```bash
conversion_factor = 9318.18
calibration_path = "path/to/calibration/files/"
calibration_clock_cycle = 25ns
```

In [Clustering4D] module, change parameters:

```bash
hist_max_cluster_charge = 15000
```

and to perform cut to exclude low ToT pixels, add:

```bash
min_pixel_charge = 2051
```

### Tips:

1.There are configuration files for masking, alignment and analysis. The adjustments can be done to all of them, or just analyse.conf alone.

2.Parameter calibration_path requires address of the directory in which the 4 files containing fit parameters are saved. Make sure they have proper file names (tot_calibration_layer_[1~4].dat) and contain all 4 times 49104 sets of parameters.

## Run corry

The script cut.cpp deals with strange lines in the benchmark run data file, which contain column number below 0. It loops the data file, throws away strange lines, and divides the file into 10 parts <benchmark_[0-9].dat> for reconstruction. 
Put it under the same directory of the raw data, compile it with ROOT and run, and 10 files <benchmark_[0-9].dat> will be created.Then go to /testbeam_analysis/march2022 and type

```bash
bash ../scripts/telescopeAlign.sh <path/to/testbeam/data/benchmark_*.dat>
```

to run corry.

