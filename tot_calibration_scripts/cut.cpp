#include <iostream>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>
using namespace std;
int main() {
	std::fstream fin;
	std::fstream fout;
	fin.open("202204082012_udp_beamonallquad_Vminus90_6Gev_kit_0_decode_cut.dat", std::ios::in);
	
	std::string textin;	
	std::string textout;

	getline(fin,textin);//skip first line

	int i;
	int j;
//	std::string s;
	for (i=0;i<=9;i++) {
		std::string filename;
//		s = std::to_string(i);
	        filename =(std::string)"benchmark_"+std::to_string(i) +(std::string) ".dat";
//       		const char *fn = filename.data();
		fout.open(filename, std::ios::out);//creat 10 files out of one
		fout<<"# PackageID;	Layer;	Column;	Row;	TS;	TS1;	TS2;	TriggerTS;	TriggerID;	ext. TS;	ext. TS2;	FIFO overflow;"<<endl;
		for (j=0;j<=7600000;j++) {
			long int packID, layer, column, row, TS, TS1, TS2, tts, triggerID,  ext1, ext2, overflow;
			if (fin.eof()){ break; }
			fin>>packID>>layer>>column>>row>>TS>>TS1>>TS2>>tts>>triggerID>>ext1>>ext2>>overflow;
		//	cout<<column<<" "<<row<<endl;
			if ((column>=0)&&(row>=0)) {
				fout<<packID<<" "<<layer<<" "<<column<<" "<<row<<" "<<TS<<" "<<TS1<<" "<<TS2<<" "<<tts<<" "<<triggerID<<" "<<ext1<<" "<<ext2<<" "<<overflow<<endl;
			}
		}
		fout.close();
		cout<<filename<<" created"<<endl;
	}
	fin.close();
	return 0;
}
