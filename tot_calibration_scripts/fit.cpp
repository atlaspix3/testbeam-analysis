#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <cmath>
#include <sstream>
#include "func.h"

void fit()
{
	TCanvas *c1 = new TCanvas("c1","c1",600,600);
	c1->SetGrid();
	std::fstream f1;
	std::fstream f2;
	f1.open("ToT_Calibration_L1_20220509.dat", std::ios::in); //open tot calibration file for check
	f2.open("ToT_Calibration_L1_20220509.dat", std::ios::in); //open tot calibration file for readin
	std::fstream fout;
	fout.open("tot_calibration_layer_1.dat", std::ios::out); //open file for output

	if(!f1.is_open()) {
		std::cerr << "Load ToT Calibration File: "
				  << "File could not be opened. This should not happen." << std::endl;
	}

	std::string text1;
	std::string text2;
	TF1 *func = new TF1("func",fitfunction,0,0.6,4); //log-plus-linear function
	func->SetParNames("lnscale","x0","linear","offset");
	func->SetParameters(5,0.21,100,50); //initialize the parameters
	func->SetParLimits(0,0,15); //lnscale
	func->SetParLimits(1,0.2,0.22); //x0, adjustments might be needed...
	func->SetParLimits(2,0,150);  //linear

	string s;
	int i = 0;
	int j = 0;
	int k = 0;
	int np;
	double voltage[9]; //larger group size will be needed if there are more data points for one pixel 
	double tot[9];
	int edm_count_0005 = 0;
	int edm_count_05 = 0;
	int edm_count_50 = 0;
	int strange_point_count = 0; //points excluded from fit
	for(i=0;i<=49104;i++){ //all 132*372 pixels
		j = 0;
		while(1) { //read in raw data
			getline(f1,text1);
			if(text1[0] == '#') {
				if(text1.find("PointCurve") != std::string::npos) {
					uint64_t start = text1.find('(');
					uint64_t ende = text1.find(')');
					s = text1.substr(start, ende - start + 1);
					cout<<"Processing Pixel "<<s<<endl;
				}
				getline(f2,text2);
 			}
			else if(text1 == "") { //end of one pixel
				getline(f2,text2);
				getline(f2,text2);
				j = 0;
				break;
			}
			else {
				double voltage_check, tot_check, error_check;
				f2 >> voltage_check >> tot_check >> error_check;
	
				if ((tot_check >= 0)&&(tot_check < 170)) { //perform some cut to the data
					voltage[j] = voltage_check;
					tot[j] = tot_check;
					j++;
					np = j;
				}
			else { strange_point_count ++;}
			}
		}
		fout<<"# Fit "<<s<<endl; //fit and write output file
		TGraph* gr = new TGraph(np);
		int q = 0;
		for (q=0;q<np;q++) {
			gr->SetPoint(q,voltage[q],tot[q]); //add the points
		}
		gr->SetMarkerStyle(3);
		c1->DrawFrame(0,0,0.6,170);
		TFitResultPtr res =  gr->Fit(func,"SQ","",0,0.6); //do the fit; adjust fit range according to input data
		TFitResult *result = res.Get();
		double edm = result->Edm();
		const double *paravalue = result->GetParams();
		const double *paraerror = result->GetErrors();
		for (k=0;k<=3;k++){
			std::string paraname = result->GetParameterName(k);
			double value = paravalue[k];
			double error = paraerror[k];
			fout<<paraname<<" "<<value<<" +- "<<error<<endl;
		}
		//cout<<edm<<endl;
		if (edm>0.005) {edm_count_0005 ++;}
		if (edm>0.5) {edm_count_05 ++;}
		if (edm>50) {edm_count_50 ++;}
		
/*		if((i==5000)||(i==7500)||(i==10000)) {	//Save plot to pdf   
			std::string filename;
			filename =(std::string) s +(std::string) ".pdf";
			const char *fn = filename.data(); 	
			gr->Draw("p");	
			gPad->SaveAs(fn);
		}*/ 
	}
	f1.close();
	f2.close();
	fout.close();

	cout<<"strange points "<<strange_point_count<<endl;
	cout<<"edm>0.005 "<<edm_count_0005<<endl;
	cout<<"edm>0.5 "<<edm_count_05<<endl;
	cout<<"edm>50 "<<edm_count_50<<endl;
}
