#include "TCanvas.h"
#include "TRandom3.h"
#include "TGraph.h"
#include "TMath.h"
#include "TArc.h"
#include "Fit/Fitter.h"
#include <Math/Functor.h>

Double_t fitfunction (Double_t *x,Double_t *par) {
	Double_t xx = x[0];
	Double_t f = 0;
	if (xx>par[1]) {
		f = par[0]*log((xx - par[1])/par[1]) + par[2] * xx + par[3];
	}
	else {
		f = 0;
	}
	//Double_t f = par[0]+par[1]*xx+par[2]*xx*xx+par[3]*xx*xx*xx;
	return f;
   
};
